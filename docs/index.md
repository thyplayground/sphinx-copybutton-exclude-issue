# sphinx-copybutton-exclude-issue

sphinx-copybutton versions: **v0.5.1**, <a href='/en/v0.5.1-go/'>v0.5.1 add '.go' to copybutton_exclude</a>, <a href='/en/v0.5.0/'>v0.5.0</a>

## v0.5.1 copies output, v0.5.0 doesn't

````{example}

```console
$ echo "11"
11
$ echo "22"
22
```
````

## Adding `.go` to exclude 

Following [DOCS: Improve docs related to text exclusion #187](https://github.com/executablebooks/sphinx-copybutton/pull/187), adding `.go` to exclude will break multi-line snippets in `v0.5.1`. 

````{example}
```console
$ cat << EOT > notes.txt
   This is an example sentence.
       Put some indentation on this line.

  EOT
```
````

These lines will have `.go` class and excluded in `v0.5.1`

```
   This is an example sentence.
       Put some indentation on this line.

  EOT
```

## My conf.py

```python
copybutton_prompt_text = r">>> |\.\.\. |\$ |In \[\d*\]: | {2,5}\.\.\.: | {5,8}: "
copybutton_prompt_is_regexp = True
copybutton_line_continuation_character = "\\"
copybutton_here_doc_delimiter = "EOT"
copybutton_selector = "div:not(.no-copybutton) > div.highlight > pre"
```
